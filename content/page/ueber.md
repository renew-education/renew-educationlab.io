---
title: Über #renewEducation
subtitle: Wieso, weshalb, warum und wer?
comments: false
---

#renewEducation ist eine Initiative von [Homeworker](https://homeworker.li), welche sich das Ziel gesetzt hat das deutsche Bildungssystem grundlegend zu sanieren.
